# Pietyping

A simple typing test to calculate your words per minute


## In-program rules (Based on Zen-mode)

  - No pre-defined text

    It's slowing you down as unless you are a copywriter
    of a dictionary without proof-reading the paragraph.
    It does not a accurate representation in trying to
    compare with real life typing.

  - For each error will give pently of -1 in correct words.

  - Can use both space and enter as a submit key

  - Cancel/Quit by pressing `esc` or let time run out



## How to install and run

```sh
# Clone it
git clone https://gitlab.com/renegadevi/Pietyping.git

# Enter directory
cd Pietyping

# Optional
python3 -m pip install virtualenv
python3 -m virtualenv venv
source venv/bin/activate

# Make sure you have required library installed
python3 -m pip install -r requirements.txt

# Make it executable
chmod +x main.py

# Run the script
./main.py
```


## License

MIT License
