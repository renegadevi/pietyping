#!/usr/bin/env python3
"""
    Pietyping

    A simple typing test to calculate your words per minute

    ---

    MIT License

    Copyright (C) 2021 Philip Andersen <philip.andersen@codeofmagi.net>

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the "Software"),
    to deal in the Software without restriction, including without limitation
    the rights to use, copy, modify, merge, publish, distribute, sublicense,
    and/or sell copies of the Software, and to permit persons to whom the
    Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
    THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
    DEALINGS IN THE SOFTWARE.

"""

import time
import os
import sys
import termios

from pynput.keyboard import Key, Listener
from threading import Timer

__author__ = "Philip Andersen <philip.andersen@codeofmagi.net>"
__license__ = "MIT"
__version__ = "0.1.0"
__copyright__ = "Copyright © 2021 Philip Andersen"


# Helper function
def ask_integer_input(message, default=0):
    """ Get user input by integer """
    while True:
        try:
            return int(
                input(f"{message} (default: {default})?: ") or str(default)
            )
        except ValueError:
            print("Not a valid number")


# Welcome screen
os.system('clear')
print("""
                   Welcome to Pietyping

 A simple typing test to calculate your words per minute

----------------------------------------------------------

  Rules:

  - No pre-defined text

    It's slowing you down as unless you are a copywriter
    of a dictionary without proof-reading the paragraph.
    It does not a accurate representation in trying to
    compare with real life typing.

  - For each error will give pently of -1 in correct words.

  - Can use both space and enter as a submit key

  - Cancel/Quit by pressing `esc` or let time run out

----------------------------------------------------------
""")


# Initial Variables
words, errors = 0, 0
first_press, finished, user_start, backspace = True, False, False, False


# Ask user for length of test
timeout = ask_integer_input("Seconds for test", default=30)
print(f"Timer set to {timeout} seconds\n")


# Tell the user the test is ready to run
print("Press `spacebar` or `enter` to start the test and start typing...\n")



def on_press(key):
    """ Main app loop """

    global words, first_press, finished, user_start, errors, backspace

    # Check if user is done with the test or want to quit
    if finished or key == Key.esc:
        termios.tcflush(sys.stdin, termios.TCIOFLUSH)
        sys.exit()

    # Check if user used backspace for error count
    if key == Key.backspace:
        backspace = True

    # Start test
    if (key == Key.space and user_start is False) or \
       (key == Key.enter and user_start is False):
        user_start = True

    # Iterate word at every space or enter event
    if key == Key.space or key == Key.enter:
        start = time.process_time()

        if first_press and user_start:
            timer = Timer(timeout, results).start()
            first_press = False

        words += 1

        if backspace:
            words -= 1
            errors += 1
            backspace = False


def on_release(key):
    pass


def results():
    """ Result message """

    global finished
    finished = True

    words_per_minute = int((words/timeout)*60)
    failure_rate     = float((words/(words+errors))*100)
    print(f"""

----------------------------------------------------------

  Results: {words_per_minute} WPM

    - Total word count: {words+errors}
    - Correct words:    {words} ({failure_rate:.1f}%)
    - Incorrect words:  {errors}

""")


with Listener(on_press=on_press,  on_release=on_release) as listener:
    listener.join()
